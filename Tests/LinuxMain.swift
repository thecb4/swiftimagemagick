import XCTest

import SwiftImageMagickTests

var tests = [XCTestCaseEntry]()
tests += SwiftImageMagickTests.allTests()
XCTMain(tests)
