# SwiftImageMagick

Based on instructions from
https://mikemikina.com/blog/watermarking-photos-with-imagemagick-vapor-3-and-swift-on-macos-and-linux/
https://oleb.net/blog/2017/12/importing-c-library-into-swift/
https://github.com/apple/swift-package-manager/blob/master/Documentation/PackageDescriptionV4_2.md#system-library-targets
https://github.com/novi/SwiftMagickWand

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Built With

* [Shelltr](https://gitlab.com/thecb4/Shelltr) - Running shell commands in swift
* [Swift-sh](https://maven.apache.org/) - SWift scripting made easy
* [ImageMagick 7](https://imagemagick.org)

### Prerequisites

* [Swift-sh](https://maven.apache.org/) - SWift scripting made easy
* [ImageMagick 7](https://imagemagick.org)

```
Give examples
```

### Installing

Package Manager

```
code steps for package manager
```

## Using

Currently limited to resizing images

```swift
import SwiftImageMagick

let path = FileManager.default.currentDirectoryPath + "/original/largeImage.jpg"
let image = try MagickImage(contentsOf: path)
image.resize(w: 80, h: 80)
try image.write(to: FileManager.default.currentDirectoryPath + "/modified/80x80.png", atomically: false)
```

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

## Roadmap and Contributing

### Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 



### Roadmap

Please read [ROADMAP](ROADMAP.md) for an outline of how we would like to evolve the library.

### Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for changes to us.

### Changes

Please read [CHANGELOG](CHANGELOG.md) for details on changes to the library. 


## Authors

* **'Cavelle Benjamin'** - *Initial work* - [thecb4](https://thecb4.io)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
