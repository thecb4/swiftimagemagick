// swift-tools-version:4.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

// https://mikemikina.com/blog/watermarking-photos-with-imagemagick-vapor-3-and-swift-on-macos-and-linux/
// https://github.com/apple/swift-package-manager/blob/263171977ebcd47f4aaca1202cff5a96c5158a64/Documentation/Usage.md#import-system-libraries

import PackageDescription

let package = Package(
  name: "SwiftImageMagick",
  products: [
    // Products define the executables and libraries produced by a package, and make them visible to other packages.
    .library(
      name: "SwiftImageMagick",
      targets: ["SwiftImageMagick"]
    )
  ],
  dependencies: [
    // Dependencies declare other packages that this package depends on.
    // .package(url: /* package url */, from: "1.0.0"),
  ],
  targets: [
    // Targets are the basic building blocks of a package. A target can define a module or a test suite.
    // Targets can depend on other targets in this package, and on products in packages which this package depends on.
    .systemLibrary(name: "imagemagick", pkgConfig: "imagemagick", providers: [.apt(["imagemagick"]), .brew(["imagemagick"])]),
    .target(
      name: "SwiftImageMagick",
      dependencies: ["imagemagick"]
    ),
    .testTarget(
      name: "SwiftImageMagickTests",
      dependencies: ["SwiftImageMagick"]
    )
  ]
)
