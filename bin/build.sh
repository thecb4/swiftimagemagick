#!/usr/bin/env bash

# LIBRARY_SEARCH_PATHS = /usr/local/Cellar/imagemagick@6/6.9.10-12/lib
# HEADER_SEARCH_PATHS = /usr/local/Cellar/imagemagick@6/6.9.10-12/include/ImageMagick-6
# OTHER_SWIFT_FLAGS = -Xcc -DMAGICKCORE_HDRI_ENABLE=0 -Xcc -DMAGICKCORE_QUANTUM_DEPTH=16

swift build --verbose \
  -Xswiftc -I/usr/local/Cellar/imagemagick/7.0.8-28/include/ImageMagick-7 \
  -Xlinker -L/usr/local/Cellar/imagemagick/7.0.8-28/lib \
  -Xlinker -lMagickWand-7.Q16HDRI \
  -Xlinker -lMagickCore-7.Q16HDRI \
  -Xcc -Xpreprocessor \
  -Xcc -fopenmp \
  -Xcc -DMAGICKCORE_HDRI_ENABLE=1 \
  -Xcc -DMAGICKCORE_QUANTUM_DEPTH=16