import XCTest
import SwiftImageMagick

final class SwiftImageMagickTests: XCTestCase {
  // func testExample() {
  //     // This is an example of a functional test case.
  //     // Use XCTAssert and related functions to verify your tests produce the correct
  //     // results.
  //     let magic = SwiftImageMagick()
  //     magic.run()
  //     XCTAssertEqual(magic.text, "Hello, World!")
  // }

  func testImageResize() {
    do {
      let path = FileManager.default.currentDirectoryPath + "/Tests/Fixtures/original/largeImage.jpg"
      let image = try MagickImage(contentsOf: path)
      image.resize(w: 80, h: 80)
      try image.write(to: FileManager.default.currentDirectoryPath + "/Tests/Fixtures/modified/80x80.png", atomically: false)
    } catch {
      print(error)
      XCTFail()
    }
  }

  static var allTests = [
    ("testImageResize", testImageResize)
  ]
}
