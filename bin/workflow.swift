#!/usr/bin/swift sh

import Foundation
import Shelltr // https://gitlab.com/thecb4/Shelltr == 0.1.10

if #available(macOS 10.13, *) {
  Shell.work {
    try Shell.assertModified(file: "CHANGELOG.md")
    // try Shell.assertModified(file: "COMMIT_MESSAGE.md")
    try Shell.rm(content: ".build", recursive: true, force: true)
    try Shell.rm(content: "DerivedData", recursive: true, force: true)
    try Shell.swiftformat()
    try Shell.swiftlint(quiet: false)

    let buildArgs = [
      "-Xswiftc", "-I/usr/local/Cellar/imagemagick/7.0.8-28/include/ImageMagick-7",
      "-Xlinker", "-L/usr/local/Cellar/imagemagick/7.0.8-28/lib",
      "-Xlinker", "-lMagickWand-7.Q16HDRI",
      "-Xlinker", "-lMagickCore-7.Q16HDRI",
      "-Xcc", "-Xpreprocessor",
      "-Xcc", "-fopenmp",
      "-Xcc", "-DMAGICKCORE_HDRI_ENABLE=1",
      "-Xcc", "-DMAGICKCORE_QUANTUM_DEPTH=16"
    ]

    try Shell.swiftbuild(arguments: buildArgs)
    try Shell.sourcekitten(module: "SwiftImageMagick", destination: Shell.cwd + "/docs/source.json")
    try Shell.jazzy()
    try Shell.gitAdd(.all)
    try Shell.gitCommit(message: "added documentation")
  }
}
