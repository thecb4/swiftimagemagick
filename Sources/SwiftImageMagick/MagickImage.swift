import imagemagick
import Foundation

public class MagickImage {
  public typealias BufferType = UInt8

  public static func genesis() {
    MagickWandGenesis()
  }

  let wand: OpaquePointer

  init() {
    wand = NewMagickWand()
  }

  public convenience init(bytes: [BufferType]) {
    self.init()
    MagickReadImageBlob(wand, bytes, bytes.count)
  }

  public convenience init(data: Data) {
    // let bytes = Array(UnsafeBufferPointer(start: UnsafePointer<UInt8>(data.bytes), count: data.length))
    // https://stackoverflow.com/questions/31821709/nsdata-to-uint8-in-swift
    let bytes = [UInt8](data)
    self.init(bytes: bytes)
  }

  public convenience init(contentsOf path: String) throws {
    // let path = FileManager.default.currentDirectoryPath + "/Tests/Fixtures/metadata.xml"
    let url = URL(fileURLWithPath: path)
    let data = try Data(contentsOf: url)
    self.init(data: data)
  }

  public func resize(w: Int, h: Int) {
    MagickResizeImage(wand, w, h, LanczosFilter)
  }

  // https://github.com/liexusong/bolt/issues/1
  // MagickWriteImageBlob has been renamed to MagickGetImageBlob in new version of Imagemagick.
  public var bytes: [BufferType] {
    var size: Int = 0
    guard let mem = MagickGetImageBlob(wand, &size) else { return [] } // TODO:
    var outBuf = Array<BufferType>.init(repeating: 0, count: size)
    for i in 0 ..< size {
      outBuf[i] = mem[i]
    }
    // TODO: free memory
    return outBuf
  }

  public var data: Data {
    // https://exceptionshub.com/nsdata-from-byte-array-in-swift.html
    var bytes = self.bytes
    return Data(buffer: UnsafeBufferPointer(start: &bytes, count: bytes.count))
  }

  // https://stackoverflow.com/questions/38198349/swift-3-0-writing-binary-image-data-with-writetofile-atomically
  public func write(to path: String, atomically: Bool) throws {
    let url = URL(fileURLWithPath: path)
    try self.data.write(to: url, options: [.atomic])
  }

  // TODO: deinit
}
